import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

//TODO Good luck !

root.render(
  <React.StrictMode>
    <h1>React Test</h1>
  </React.StrictMode>
);